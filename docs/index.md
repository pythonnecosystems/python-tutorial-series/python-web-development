# Python 웹 개발: Flask, Django 프레임워크 <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 Flask와 Django 프레임워크를 사용하여 웹 어플리케이션을 개발하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./web-development.md#intro)
1. [Flask 기초](./web-development.md#sec_02)
1. [Flask 템플릿과 양식](./web-development.md#sec_03)
1. [Flask 데이터베이스와 인증](./web-development.md#sec_04)
1. [Flask RESTful API와 테스트](./web-development.md#sec_05)
1. [Django 기초](./web-development.md#sec_06)
1. [Django 모델과 관리자](./web-development.md#sec_07)
1. [Django 뷰와 템플릿](./web-development.md#sec_08)
1. [Django 양식과 사용자 관리](./web-development.md#sec_09)
1. [Django REST 프레임워크와 배포](./web-development.md#sec_10)

<a name="footnote_1">1</a>: [Python Tutorial 38 — Python Web Development: Flask, Django Frameworks](https://python.plainenglish.io/python-tutorial-38-python-web-development-flask-django-frameworks-4c20b371d668?sk=c73fd0cd3f9156c1671b2da9b2cc9c75)를 편역하였습니다.
